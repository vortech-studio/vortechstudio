<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $appends = ['status_label'];

    public function news()
    {
        return $this->hasMany(News::class);
    }

    public function getStatusLabelAttribute()
    {
        return match ($this->status) {
            "idea" => "<span class='badge bg-amber-500'><i class='fa-solid fa-flask'></i> Idée</span>",
            "develop" => "<span class='badge bg-primary'><i class='fa-solid fa-code'></i> En développement</span>",
            "finish" => "<span class='badge bg-success'><i class='fa-solid fa-check'></i> Production</span>",
        };
    }
}
