<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $casts = [
        'published_at' => 'datetime',
    ];
    protected $guarded = [];

    public function game()
    {
        return $this->belongsTo(Game::class);
    }
}
