<?php

namespace App\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Select extends Component
{
    /**
     * Constructs a new instance of the class.
     *
     * @param string $name The name of the instance.
     * @param string $label The label of the instance.
     * @param array $cases The cases of the instance. [label, value]
     * @param string $selectPlaceholder The select placeholder of the instance.
     * @param string|null $value The value of the instance.
     * @param bool $required The required flag of the instance.
     */

    public function __construct(
        public string $name,
        public string $label,
        public array $cases,
        public string $selectPlaceholder = '',
        public ?string $value = null,
        public bool $required = false
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.form.select');
    }
}
