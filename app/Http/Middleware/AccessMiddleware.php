<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AccessMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if(!$request->user()->access) {
            abort(401, "Vous n'avez pas accès à cette interface. Veuillez contacter un administrateur à support@vortechstudio.fr en spéficiant votre nom d'utilisateur, et l'accès demander.");
        }

        return $next($request);
    }
}
