<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\News;

class HomeController extends Controller
{
    public function index()
    {
        return view('index', [
            "games" => Game::all(),
            "news" => News::where('published', true)->orderBy('published_at', 'desc')->limit(5)->get(),
            "slides" => News::where('published', true)->where('slideshow', true)->orderBy('published_at', 'desc')->limit(5)->get(),
        ]);
    }

    public function news()
    {
        return view('news');
    }

    public function newsShow($slug)
    {
        return view('news-show', [
            "slug" => $slug
        ]);
    }

    public function project()
    {
        return view('game');
    }

    public function projectShow($slug)
    {
        return view('game-show', [
            "slug" => $slug,
            'name' => str_replace('-', ' ', $slug)
        ]);
    }
}
