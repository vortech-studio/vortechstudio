<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function preview(Request $request)
    {
        $content = json_decode($request->getContent(), true);
        return view('admin.preview', ['blocs' => $content]);
    }
}
