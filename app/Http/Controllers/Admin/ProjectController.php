<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index()
    {
        return view('admin.projects.index', [
            'games' => \App\Models\Game::all(),
        ]);
    }

    public function create()
    {
        return view('admin.projects.create');
    }
}
