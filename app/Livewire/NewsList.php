<?php

namespace App\Livewire;

use App\Models\News;
use Illuminate\Support\Collection;
use Livewire\Component;

class NewsList extends Component
{
    public Collection $news;
    public int $pageNumber = 1;

    public bool $hasMorePage;

    public function mount(): void
    {
        $this->news = new Collection();
        $this->loadMore();
    }
    public function loadMore()
    {
        $news = News::where('published', true)
            ->orderBy('published_at', 'desc')
            ->paginate(4, ['*'], 'page', $this->pageNumber);
        $this->pageNumber++;
        $this->hasMorePage = $news->hasMorePages();
        $this->news->push(...$news->items());
    }
    public function render()
    {
        return view('livewire.news-list');
    }
}
