<?php

namespace App\Livewire\Auth;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Livewire\Attributes\Validate;
use Livewire\Component;

class Register extends Component
{
    #[Validate('required|email')]
    public string $email;
    #[Validate('required|min:8|confirmed')]
    public string $password;
    #[Validate('required|string|min:3')]
    public string $name;
    public string $password_confirmation;

    public function register()
    {
        $this->validate();
        $user = User::create([
            "name" => $this->name,
            "email" => $this->email,
            "password" => \Hash::make($this->password),
        ]);

        event(new Registered($user));

        return redirect()->intended();
    }

    public function render()
    {
        return view('livewire.auth.register');
    }
}
