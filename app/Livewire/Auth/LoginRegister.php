<?php

namespace App\Livewire\Auth;

use Livewire\Component;

class LoginRegister extends Component
{
    public function render()
    {
        return view('livewire.auth.login-register');
    }
}
