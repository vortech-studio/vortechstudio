<?php

namespace App\Livewire\Auth;

use Livewire\Attributes\Validate;
use Livewire\Component;

class Login extends Component
{
    #[Validate('required|email')]
    public string $email = '';
    #[Validate('required|string')]
    public string $password = '';
    public function login() {
        $this->validate();
        if(\Auth::attempt($this->only('email', 'password'))) {
            return redirect()->intended();
        } else {
            session()->flash("danger", "Identifiant invalide");
        }
    }
    public function render()
    {
        return view('livewire.auth.login');
    }
}
