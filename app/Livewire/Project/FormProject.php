<?php

namespace App\Livewire\Project;

use App\Models\Game;
use Livewire\Attributes\Validate;
use Livewire\Component;

class FormProject extends Component
{
    public int $projectId;
    #[Validate('required|min:3')]
    public string $name = '';
    #[Validate('required|url')]
    public string $link_site = '';
    #[Validate('required')]
    public string $synopsis = '';
    #[Validate('required')]
    public string $description = '';
    public string $latest_version = '';
    public string $status = '';
    public bool $is_game;

    public array $cases = [
        [
            "label" => "Idée",
            "value" => "idea"
        ],
        [
            "label" => "En développement",
            "value" => "develop"
        ],
        [
            "label" => "Production",
            "value" => "finish"
        ],
    ];

    public function mount()
    {
        if (Game::where('id', request()->get('id'))->exists()) {
            $this->projectId = request()->get('id');
            $this->name = Game::find(request()->get('id'))->name;
            $this->link_site = Game::find(request()->get('id'))->link_site;
            $this->synopsis = Game::find(request()->get('id'))->synopsis;
            $this->description = Game::find(request()->get('id'))->description;
            $this->latest_version = Game::find(request()->get('id'))->latest_version;
            $this->status = Game::find(request()->get('id'))->status;
            $this->is_game = Game::find(request()->get('id'))->is_game;
        }
    }

    public function store()
    {
        $this->validate();
        try {
            Game::create([
                "name" => $this->name,
                "link_site" => $this->link_site,
                "synopsis" => $this->synopsis,
                "description" => $this->description,
                "latest_version" => $this->latest_version,
                "status" => $this->status,
                "is_game" => $this->is_game
            ]);
        } catch (\Exception $exception) {
            dd($exception);
        }

        toastr()->success("Le projet à été créer avec succès", "OK");
    }

    public function update()
    {
        $this->validate();
        try {
            Game::where('id', request()->get('id'))->update([
                "name" => $this->name,
                "link_site" => $this->link_site,
                "synopsis" => $this->synopsis,
                "description" => $this->description,
                "latest_version" => $this->latest_version,
                "status" => $this->status,
                "is_game" => $this->is_game,
            ]);
        }catch (\Exception $exception) {
            dd($exception);
        }

        toastr()->success("Le projet a été mis à jour avec succès", "OK");
    }

    public function render()
    {
        return view('livewire.project.form-project', [
            'cases' => $this->cases,
        ]);
    }
}
