<?php

namespace App\Livewire\Project;

use App\Models\Game;
use Livewire\Attributes\Url;
use Livewire\Component;
use Livewire\WithPagination;

class TableProject extends Component
{
    use WithPagination;

    #[Url]
    public string $search = '';
    public string $orderField = 'name';
    public string $orderDirection = 'ASC';

    protected function queryString()
    {
        return [
            "search" => ['except' => ''],
            "orderField" => ['except' => 'name'],
            "orderDirection" => ['except' => 'ASC']
        ];
    }

    public function setOrderField(string $name)
    {
        if ($name === $this->orderField) {
            $this->orderDirection = $this->orderDirection === 'ASC' ? 'DESC' : 'ASC';
        } else {
            $this->orderField = $name;
            $this->reset('orderDirection');
        }
    }

    public function render()
    {
        return view('livewire.project.table-project', [
            "games" => Game::where('name', 'like', '%' . $this->search . '%')
                ->orderBy($this->orderField, $this->orderDirection)
                ->paginate(5)
        ]);
    }
}
