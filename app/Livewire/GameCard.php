<?php

namespace App\Livewire;

use App\Models\Game;
use Livewire\Component;

class GameCard extends Component
{

    public $slug;
    public $name;

    public function mount()
    {
        $this->slug = explode('-', $this->slug);
        $this->name = implode(' ', $this->slug);
    }
    public function render()
    {
        return view('livewire.game-card', [
            "game" => Game::where('name', $this->name)->first(),
        ]);
    }
}
