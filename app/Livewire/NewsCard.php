<?php

namespace App\Livewire;

use App\Models\News;
use Livewire\Component;

class NewsCard extends Component
{
    public $slug;
    public $title;

    public function mount()
    {
        $this->slug = explode('-', $this->slug);
        $this->title = implode(' ', $this->slug);
    }
    public function render()
    {
        return view('livewire.news-card', [
            "post" => News::where('title', $this->title)->first()
        ]);
    }
}
