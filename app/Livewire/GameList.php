<?php

namespace App\Livewire;

use App\Models\Game;
use Illuminate\Support\Collection;
use Livewire\Component;

class GameList extends Component
{
    /**
     * Renders the view for the given PHP function.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.game-list', [
            "games" => Game::all()
        ]);
    }
}
