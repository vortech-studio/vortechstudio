<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('link_site');
            $table->string('synopsis');
            $table->longText("description");
        });

        switch (config('app.env')) {
            case 'local':
                \App\Models\Game::create(["name" => "Railway Manager", "link_site" => "https://railway-manager.test", "synopsis" => "Gérez votre empire ferroviaire", "description" => "Gérez votre empire ferroviaire"]);
                \App\Models\Game::create(["name" => "Vortech Lab", "link_site" => "https://lab.vortechstudio.test", "synopsis" => "Interface social de Vortech Studio", "description" => "Partager et consulter, la base de donnée relationnel de Vortech Studio"]);
                break;
            case 'testing':
                \App\Models\Game::create(["name" => "Railway Manager", "link_site" => "https://railway-manager.dev", "synopsis" => "Gérez votre empire ferroviaire", "description" => "Gérez votre empire ferroviaire"]);
                \App\Models\Game::create(["name" => "Vortech Lab", "link_site" => "https://lab.vortechstudio.dev", "synopsis" => "Interface social de Vortech Studio", "description" => "Partager et consulter, la base de donnée relationnel de Vortech Studio"]);
                break;
            case 'production':
                \App\Models\Game::create(["name" => "Railway Manager", "link_site" => "https://railway-manager.ovh", "synopsis" => "Gérez votre empire ferroviaire", "description" => "Gérez votre empire ferroviaire"]);
                \App\Models\Game::create(["name" => "Vortech Lab", "link_site" => "https://lab.vortechstudio.fr", "synopsis" => "Interface social de Vortech Studio", "description" => "Partager et consulter, la base de donnée relationnel de Vortech Studio"]);
        }
    }

    public function down()
    {
        Schema::dropIfExists('games');
    }
};
