<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('games', function (Blueprint $table) {
            $table->string('latest_version')->nullable();
            $table->enum('status', ['idea', 'develop', 'finish'])->default("idea");
            $table->boolean('is_game')->default(false);
        });
    }

    public function down(): void
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn(['latest_version', 'status', 'is_game']);
        });
    }
};
