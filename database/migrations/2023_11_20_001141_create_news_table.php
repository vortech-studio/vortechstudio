<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['announcement', 'event', 'news'])->default('news');
            $table->string('title');
            $table->string('synopsis')->nullable();
            $table->longText('contenue');
            $table->boolean('published')->default(0);
            $table->boolean('slideshow')->default(0);
            $table->timestamp('published_at')->nullable();
            $table->timestamps();

            $table->foreignId('game_id')
               ->nullable()
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('news');
    }
};
