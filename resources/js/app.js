import './bootstrap';
import '../scss/styles.scss'
import 'uikit/dist/js/uikit.min.js'
import 'uikit/dist/js/uikit-icons.min.js'
import 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js'
import lozad from 'lozad'
import Alpine from 'alpinejs'
import * as bootstrap from 'bootstrap'


const observer = lozad(); // lazy loads elements with default selector as '.lozad'
observer.observe();

window.Alpine = Alpine
import './visual-component.js'

