import {HTMLText, Repeater, Row, Select, Text, VisualEditor, FR, Checkbox} from "@boxraiser/visual-editor";


let editor = new VisualEditor({
    lang: FR
});
editor.registerComponent('hero', {
    title: 'Hero',
    category: 'Banner',
    fields: [
        Text('title', {
            multiline: false,
            label: 'Titre'
        }),
        Text('subtitle', {
            multiline: false,
            label: 'Sous Titre'
        }),
        Text('media', {
            multiline: false,
            label: 'Video',
            help: 'Vidéo Youtube ou Vimeo'
        }),
        HTMLText('content', {
            label: 'Contenu',
            multiline: true
        }),
        Repeater('buttons', {
            title: 'Boutons',
            addLabel: 'Nouveau Bouton',
            fields: [
                Text('label', { label: 'Label', default: 'Call to action' }),
                Text('url', { label: 'Link' }),
                Select('type', {
                    default: 'primary',
                    label: 'type',
                    options: [
                        { label: 'Primaire', value: 'primary' },
                        { label: 'Erreur', value: 'danger' },
                        { label: 'Attention', value: 'warning' },
                        { label: 'OK', value: 'success' },
                        { label: 'Information', value: 'info' },
                        { label: 'Secondaire', value: 'secondary' }
                    ]
                }),
                Checkbox('outline', {
                    label: 'Outline',
                    default: false,
                })
            ]
        })
    ]
})

editor.defineElement();
