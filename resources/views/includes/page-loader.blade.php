<div class="bg-blue-700 d-flex flex-column justify-content-center align-items-center m-auto p-auto" style="width: 100%; height: 100%">
    <h1>Cube Flipping Loader</h1>
    <div class="cube-wrapper">
        <div class="cube-folding">
            <span class="leaf1"></span>
            <span class="leaf2"></span>
            <span class="leaf3"></span>
            <span class="leaf4"></span>
        </div>
        <span class="loading" data-name="Loading">Chargement</span>
    </div>
</div>
