@extends("template")

@section("css")

@endsection

@section("content")
    <div class="uk-height-medium uk-flex uk-flex-start uk-flex-middle uk-background-cover uk-light" data-src="{{ Storage::url('/other/wall_2.png') }}" uk-img="loading: eager">
        <div class="container">
            <h1>Actualités</h1>
        </div>
    </div>
    <div class="container mt-5">
        <livewire:news-list />
    </div>
@endsection

@section("js")
@endsection
