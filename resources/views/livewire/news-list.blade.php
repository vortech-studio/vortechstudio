<div>
    <div class="d-flex flex-wrap justify-content-around align-items-center js-filter mb-2">
        @foreach($news as $new)
            <a href="{{ route('news.show', Str::slug($new->title)) }}" class="card shadow-lg products text-decoration-none mb-5" style="width: 45%">
                <div class="position-relative">
                    <span class="badge bg-primary position-absolute top-0 end-0 m-2">Produits</span>
                </div>
                <div class="img-hover-zoom">
                    <img src="{{ Storage::url('/news/'.$new->published_at->year.'/'.$new->published_at->month.'/'.$new->id.'.webp') }}" class="card-img-top" alt="...">
                </div>
                <div class="card-body">
                    <span class="text-grey-600 fw-light">{{ $new->published_at->format('d/m/Y') }}</span>
                    <div class="text-dark fw-semibold fs-3">{{ $new->title }}</div>
                    <div class="fs-7 text-grey-500 fst-italic">{{ $new->synopsis }}</div>
                </div>
            </a>
        @endforeach
    </div>
    @if($hasMorePage)
    <div class="d-flex flex-wrap justify-content-center">
        <button class="btn btn-lg btn-outline-primary rounded-5 w-250px" wire:click="loadMore" wire:loading.attr="disabled">
            <span wire:loading.remove>Plus</span>
            <div wire:loading><span uk-spinner></span> Chargement...</div>
        </button>
    </div>
    @endif
</div>

