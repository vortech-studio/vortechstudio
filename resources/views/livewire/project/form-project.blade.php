<div class="card shadow-lg mt-5">
    <div class="card-header">
        <div class="card-title">Nouveau Projet</div>
    </div>
    <form class="card-body" @if($projectId != null) wire:submit.prevent="update" @else wire:submit.prevent="store" @endif >
        <x-form.input
            name="name"
            label="Nom du projet"
            required
            wire:model="name"/>

        <x-form.input
            name="link_site"
            label="Lien du site"
            type="url"
            required
            wire:model="link_site"
        />

        <x-form.textarea
            name="synopsis"
            label="Courte description du projet"
            required
            wire:model="synopsis"
        />

        <div x-data="{open: false}">
            <div class="my-5">
                <span class="fw-bold">Editer le contenue</span>
                <button type="button" @click="open = true" class="btn btn-sm btn-primary rounded-5"><i class="fa-solid fa-edit"></i></button>
            </div>
            <visual-editor
                :hidden="open === false"
                name="description"
                value="{{ $description }}"
                @close="open = false"
            ></visual-editor>
        </div>

        <div class="row">
            <div class="col-2">
                <x-form.check-switch
                    name="is_game"
                    label="Est un jeux"
                    value="1"
                    wire:model="is_game"
                    checked />
            </div>
            <div class="col-6">
                <x-form.select
                    name="status"
                    label="Status actuel du projet"
                    :cases="$cases"
                    wire:model="status"
                    value="{{ $status }}" />
            </div>
            <div class="col-4">
                <x-form.input
                    name="latest_version"
                    label="Dernière version"
                    wire:model="latest_version"
                    />
            </div>
        </div>
        <div class="d-flex justify-content-center mt-5">
            <button type="submit" class="btn btn-outline-primary" style="width: 80%">
                <span wire:loading.remove>Valider</span>
                <span wire:loading><i class="fa-solid fa-spinner fa-spin me-1"></i> En cours...</span>
            </button>
        </div>

    </form>
</div>
