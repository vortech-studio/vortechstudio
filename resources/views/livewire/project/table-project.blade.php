<div>
    <div class="d-flex flex-row justify-content-between align-items-center mb-3">
        <input type="text" class="form-control w-300px" placeholder="Rechercher un projet" wire:model.live.debounce.500ms="search">
        <div>
            <a href="{{ route('admin.projects.create') }}" class="btn btn-primary rounded-5"><i class="ti ti-circle-plus"></i> Nouveau projet</a>
        </div>
    </div>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <x-table-header :direction="$orderDirection" name="name" :field="$orderField">Projet</x-table-header>
                <x-table-header :direction="$orderDirection" name="is_game" :field="$orderField">Est un jeux</x-table-header>
                <x-table-header :direction="$orderDirection" name="status" :field="$orderField">Status</x-table-header>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($games as $game)
                <tr>
                    <td>{{ $game->name }}</td>
                    <td>
                        @if($game->is_game)
                            <i class="ti ti-circle-check text-success" data-bs-toggle="tooltip" title="Oui"></i>
                        @else
                            <i class="ti ti-circle-x text-danger" data-bs-toggle="tooltip" title="Non"></i>
                        @endif
                    </td>
                    <td>{!! $game->status_label !!}</td>
                    <td></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $games->links() }}
</div>
