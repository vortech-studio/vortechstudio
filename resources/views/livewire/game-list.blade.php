<div class="d-flex flex-wrap justify-content-center align-items-center">
    @foreach($games as $game)
        <a href="{{ route('project.show', Str::slug($game->name)) }}" class="card shadow-lg w-300px h-430px bgi-position-center bgi-size-cover bgi-hoverable" style="background-image: url({{ Storage::url('/games/wall_'.Str::snake($game->name).'.webp') }});"></a>
    @endforeach
</div>
