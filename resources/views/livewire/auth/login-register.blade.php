<div class="d-flex flex-column my-20">
    <div class="d-flex justify-content-center mb-25">
        <span class="fs-1 fw-bold text-gradient-blue text-decoration-underline">Bienvenue sur Vortech Studio</span>
    </div>
    <div class="container">
        <x-base.alert type="info" icon="info-circle" message="La connexion par cette adresse vous donnes accès à l'interface administrative de vortech studio. Une étude de votre demande sera effectuer lors de votre inscription."/>
        @if(session()->has('danger') || session()->has('warning'))
            <x-base.alert
                :type="session()->has('danger') ? 'danger' : 'warning'"
                :icon="session()->has('danger') ? 'xmark-circle' : 'exclamation-triangle'"
                :message="session()->has('danger') ? session('danger') : session('warning')" />
        @endif
        <div class="d-flex flex-row justify-content-around gap-3">
            <livewire:auth.login />
            <div class="vr"></div>
            <livewire:auth.register />
        </div>
    </div>
</div>
