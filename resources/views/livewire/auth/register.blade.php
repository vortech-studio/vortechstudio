<div style="width: 100%;">
    <div class="bg-gradient-dark-blue rounded-2 p-5 shadow-lg text-white">
        <h1 class="text-white mb-5">Enregistrement</h1>
        <form action="" wire:submit.prevent="register">
            @csrf
            <div class="mb-5">
                <label for="name" class="form-label">Votre nom <span class="text-danger">*</span></label>
                <input type="text" wire:model="name" id="name" class="form-control" name="name" placeholder="Votre nom / pseudo" required autofocus>
            </div>
            <div class="mb-5">
                <label for="email" class="form-label">Adresse Mail <span class="text-danger">*</span></label>
                <input type="email" id="email" wire:model="email" class="form-control" name="email" placeholder="Votre adresse mail" required autofocus>
            </div>
            <div class="mb-5">
                <label for="password" class="form-label">Mot de passe <span class="text-danger">*</span></label>
                <input type="password" id="password" wire:model="password" class="form-control" name="password" placeholder="Votre mot de passe" required autofocus>
            </div>
            <div class="mb-5">
                <label for="password_confirmation" class="form-label">Confirmer le Mot de passe <span class="text-danger">*</span></label>
                <input type="password" wire:model="password_confirmation" id="password_confirmation" class="form-control" name="password_confirmation" placeholder="Confirmer le mot de passe" required autofocus>
            </div>
            <button type="submit" class="btn btn-lg btn-outline-light" style="width: 100%;">
                <span wire:loading.remove>Valider</span>
                <span class="" wire:loading>
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Veuillez patienter...
                </span>
            </button>
        </form>
    </div>
</div>
