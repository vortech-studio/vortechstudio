<div>
    @dump($paginator)
    @if($paginator)
        <nav>
            <ul class="pagination">
                <li class="page-item disabled">
                    <a class="page-link">Précédent</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">Suivant</a>
                </li>
            </ul>
        </nav>
    @endif
</div>
