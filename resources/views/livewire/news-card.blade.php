<div>
    <div class="d-flex flex-column align-items-center">
        <div class="fs-1 fw-bold mb-3">{{ $post->title }}</div>
        <div class="d-flex flex-row justify-content-around mb-2">
            <span class="badge bg-bluegrey-400 me-5">Produit</span>
            <span>{{ $post->published_at->format("d/m/Y") }}</span>
        </div>
        <div class="hr"></div>
        <div class="d-flex flex-column align-items-center justify-content-center" style="width: 100%;">
            <div class="d-flex flex-row justify-content-around align-items-center mb-2">
                <button class="btn rounded-5 me-1 btn-hoverable btn-facebook"><i class="fa-brands fa-facebook text-white"></i> </button>
                <button class="btn rounded-5 me-1 btn-hoverable btn-twitter"><i class="fa-brands fa-twitter text-white"></i> </button>
                <button class="btn rounded-5 me-1 btn-hoverable btn-youtube"><i class="fa-brands fa-youtube text-white"></i> </button>
                <button class="btn rounded-5 bg-grey-200 me-1 btn-hoverable">
                    <img src="{{ Storage::url('/logos/logo_carre.png') }}" class="w-15px" alt="">
                </button>
            </div>
            <p class="fst-italic">{!! $post->synopsis !!}</p>
            <img src="{{ Storage::url('/news/'.$post->published_at->year.'/'.$post->published_at->month.'/'.$post->id.'.webp') }}" style="width: 75%;" class="rounded" alt="">
            <div class="my-5" style="width: 75%;">
                {!! $post->contenue !!}
            </div>
        </div>
    </div>
</div>
