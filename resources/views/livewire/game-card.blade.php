<div class="m-0">
    <div class="h-320px uk-flex uk-flex-start uk-flex-middle uk-background-cover uk-light " data-src="{{ Storage::url('/games/wall_'.Str::snake($name).'.webp') }}" uk-img="loading: eager">
        <div class="container">
            <div class="d-flex flex-row align-items-center">
                <button class="btn btn-hoverable rounded-5 bg-grey-300 me-5" onclick="window.history.back()"><i class="fa-solid fa-arrow-left"></i> </button>
                <div class="d-flex flex-column flex-grow-1">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ Storage::url('/games/logo_'.Str::snake($name).'.webp') }}" alt="" class="w-120px me-3">
                        <div class="d-flex flex-column">
                            <span class="fs-2 fw-bolder text-light">{{ Str::title($name) }}</span>
                            <div class="d-flex flex-row bg-opacity-75 bg-light rounded-1 p-2">
                                @if($game->is_game)
                                <div>
                                    <span class="text-dark">Status:</span>
                                    {!! $game->status_label !!}
                                </div>
                                <div class="vr mx-1 border-start border-black"></div>
                                <div>
                                    <span class="text-dark">Version:</span>
                                    <span class="badge bg-primary">{{ $game->latest_version }}</span>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-column">
                    @if($game->is_game)
                    <a href="https://auth.{{ config('app.domain') }}/login?redirect={{ Str::snake($game->name) }}" class="btn btn-lg btn-primary rounded-5 w-250px mb-1">Jouer</a>
                    @endif
                    <a href="{{ $game->link_site }}" class="btn btn-lg btn-secondary rounded-5 w-250px">Accès au site</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-gradient-amber p-30 shadow-box mb-5">
        <div class="container">
            <div class="d-flex flex-row justify-content-between align-items-center">
                <div class="d-flex flex-column me-5" style="width: 50%;">
                    <h1 class="fw-bold text-decoration-underline text-white">Gestion de la compagnie</h1>
                    <small class="text-end text-white fs-7 fst-italic">Composant de la version 0.4.0</small>
                    <div class="hr" style="width: 100%"></div>
                    <p class="text-white fs-3">Gérez les informations globales de votre compagnie et bien plus avec la nouvelle mise à jour 0.4.0 de Railway Manager.</p>
                </div>
                <div class="d-flex flex-column align-items-center" style="width: 50%">
                    <div class="border border-3 border-grey-300 rounded-3 m-3 overflow-hidden" style="width: 100%;">
                        <iframe height="315" style="width: 100%; height: 315px" src="https://www.youtube.com/embed/hW0o2SvzUTY?si=83h0cG8Lu7Uanako" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen uk-responsive uk-video="autoplay: false"></iframe>
                    </div>
                    <button class="btn btn-lg btn-outline-warning rounded-5" style="width: 40%;">
                        Plus d'Info
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="container d-flex flex-column justify-content-center align-items-center mb-5">
        {!! $game->synopsis !!}
        <div class="hr"></div>
        {!! $game->description !!}
    </div>

    <div class="container-fluid shadow-box bg-gradient-amber mb-0">
        <div class="text-center fs-1 text-white fw-bolder my-5 py-3">Actualités Récentes</div>
        <div class="container">
            <div class="uk-slider-container-offset mb-5" uk-slider="finite: false">
                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                    <ul class="uk-slider-items uk-child-width-1-4@s uk-grid">
                        @foreach($game->news()->where('published', true)->orderBy('published_at', 'desc')->limit(8)->get() as $new)
                            <li>
                                <a href="{{ route('news.show', Str::slug($new->title)) }}" class="card shadow-lg products text-decoration-none mb-5">
                                    <div class="position-relative">
                                        <span class="badge bg-primary position-absolute top-0 end-0 m-2">Produits</span>
                                    </div>
                                    <div class="img-hover-zoom">
                                        <img src="{{ Storage::url('/news/'.$new->published_at->year.'/'.$new->published_at->month.'/'.$new->id.'.webp') }}" class="card-img-top" alt="...">
                                    </div>
                                    <div class="card-body">
                                        <span class="text-grey-600 fw-light">{{ $new->published_at->format('d/m/Y') }}</span>
                                        <div class="text-dark fw-semibold fs-3">{{ $new->title }}</div>
                                        <div class="fs-7 text-grey-500 fst-italic">{{ $new->synopsis }}</div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href uk-slidenav-previous uk-slider-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href uk-slidenav-next uk-slider-item="next"></a>
                </div>
                <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
            </div>
        </div>
    </div>
</div>
