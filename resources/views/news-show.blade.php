@extends("template")

@section("css")

@endsection

@section("content")
    <div class="h-200px uk-flex uk-flex-start uk-flex-middle uk-background-cover uk-light" data-src="{{ Storage::url('/other/wall_2.png') }}" uk-img="loading: eager">
        <div class="container">
            <button class="btn btn-hoverable rounded-5 bg-grey-300" onclick="window.history.back()"><i class="fa-solid fa-arrow-left"></i> </button>
        </div>
    </div>
    <div class="container mt-5">
        <livewire:news-card :slug="$slug" />
    </div>
@endsection

@section("js")
@endsection
