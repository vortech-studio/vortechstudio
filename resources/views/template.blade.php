<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>{{ config('app.name') }}</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700"/>
    @vite(['resources/css/app.css'])
    @yield("css")

</head>
<body>
    <header data-bs-theme="dark">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark h-60px">
            <div class="container-fluid px-20">
                <a class="navbar-brand" href="{{ route('home') }}">

                    <img src="{{ Storage::url('/logos/logo_long_multi.png') }}" alt="" class="h-45px">
                </a>
                <div class="d-flex">
                    <ul class="navbar-nav me-auto mb-2 mb-md-0 fs-4">
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('home') ? 'active' : '' }}" href="{{ route('home') }}">Acceuil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('news*') ? 'active' : '' }}" href="{{ route('news') }}">Actualités</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('project*') ? 'active' : '' }}" href="{{ route('project') }}">Projet</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('support') }}">Support</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main>
        @yield("content")
    </main>
    <footer class="container-fluid bg-gradient-dark-blue h-auto mt-0 pt-2">
        <div class="d-flex flex-row justify-content-center align-items-center mb-2">
            <img src="{{ Storage::url('/logos/logo_long_multi.png') }}" class="w-100px" alt="">
        </div>
        <div class="d-flex flex-row justify-content-center">
            <a href="" class="btn btn-link text-white fs-6 text-decoration-none">Acceuil</a>
            <div class="vr mx-2 text-white border-start rounded" style="width: 3px;"></div>
            <a href="" class="btn btn-link text-white fs-6 text-decoration-none">Actualités</a>
            <div class="vr mx-2 text-white border-start rounded" style="width: 3px;"></div>
            <a href="" class="btn btn-link text-white fs-6 text-decoration-none">Projets</a>
            <div class="vr mx-2 text-white border-start rounded" style="width: 3px;"></div>
            <a href="" class="btn btn-link text-white fs-6 text-decoration-none">Support</a>
            <div class="vr mx-2 text-white border-start rounded" style="width: 3px;"></div>
            <a href="" class="btn btn-link text-white fs-6 text-decoration-none">Politique de confidentialité</a>
            <div class="vr mx-2 text-white border-start rounded" style="width: 3px;"></div>
            <a href="" class="btn btn-link text-white fs-6 text-decoration-none">Accord Utilisateur</a>
        </div>
        <div class="d-flex flex-row justify-content-center mt-5">
            <span class="text-white fst-italic fs-7">Copyright © VORTECH STUDIO. All Rights Reserved.</span>
        </div>
    </footer>
    @vite(['resources/js/app.js'])
    @yield("js")
</body>
</html>
