<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>{{ config('app.name') }}</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700"/>
    <link rel="stylesheet" href="{{ asset('/admin/css/styles.min.css') }}">
    @vite(['resources/css/app.css'])
</head>

<body>
<!--  Body Wrapper -->
<div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
     data-sidebar-position="fixed" data-header-position="fixed">
    @include("admin.includes.sidebar")
    <!--  Main wrapper -->
    <div class="body-wrapper">
        @include("admin.includes.header")
        <div class="container-fluid">
            @yield("content")
        </div>
    </div>
</div>
<script src="{{ asset('/admin/libs/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/admin/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('/admin/js/sidebarmenu.js') }}"></script>
<script src="{{ asset('/admin/js/app.min.js') }}"></script>
<script src="{{ asset('/admin/libs/simplebar/dist/simplebar.js') }}"></script>
@vite(['resources/js/app.js'])
@yield("scripts")
</body>

</html>
