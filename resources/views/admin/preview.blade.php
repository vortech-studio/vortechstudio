@extends('admin.template')

@section("content")
    <div id="ve-components">
        @foreach($blocs as $bloc)
            @include("admin.includes.{$bloc['_name']}", $bloc)
        @endforeach
    </div>
@endsection
