<div class="container-fluid bg-gradient-amber p-30 shadow-box mb-5">
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center">
            <div class="d-flex flex-column me-5" style="width: 50%;">
                <h1 class="fw-bold text-decoration-underline text-white">{{ $title }}</h1>
                <small class="text-end text-white fs-7 fst-italic">{{ $subtitle }}</small>
                <div class="hr" style="width: 100%"></div>
                <p class="text-white fs-3">{!! $content !!}</p>
            </div>
            <div class="d-flex flex-column align-items-center" style="width: 50%">
                <div class="border border-3 border-grey-300 rounded-3 m-3 overflow-hidden" style="width: 100%;">
                    <iframe height="315" style="width: 100%; height: 315px" src="{{ $media }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen uk-responsive uk-video="autoplay: false"></iframe>
                </div>
                @foreach($buttons as $button)
                    <a href="{{ $button['url'] }}" class="btn btn-lg @if($button['outline']) btn-outline-{{ $button['type'] }} @else btn-{{ $button['type'] }} @endif rounded-5" style="width: 40%;">
                        {{ $button['label'] }}
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
