@extends('admin.template')

@section("content")
    <div class="card shadow-lg">
        <div class="card-header">
            <div class="card-title"><i class="ti ti-device-gamepad"></i> Liste des projects & jeux</div>
        </div>
        <div class="card-body">
            <livewire:project.table-project :games="$games" />
        </div>
        <div class="card-footer">

        </div>
    </div>
@endsection

@section("scripts")

@endsection
