@extends('admin.template')

@section("content")
    <div class="d-flex flex-row align-items-center">
        <a href="{{ route('admin.projects') }}" class="btn btn-secondary me-2 rounded-5"><i class="ti ti-arrow-back"></i></a>
        <span class="fw-bolder fs-2"><i class="ti ti-flask"></i> Nouveau Projet</span>
    </div>
    <livewire:project.form-project />
@endsection

@section("scripts")

@endsection
