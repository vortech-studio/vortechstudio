@extends("template")

@section("css")

@endsection

@section("content")
    <div class="uk-position-relative uk-visible-toggle uk-light shadow-box mb-5" tabindex="-1" uk-slideshow="animation: pull;autoplay: true">

        <ul class="uk-slideshow-items">
            @foreach($slides as $slide)
            <li>
                <img src="{{ Storage::url('/news/'.$slide->created_at->year.'/'.$slide->created_at->month.'/'.$slide->id.'.webp') }}" alt="" uk-cover>

                <div class="uk-overlay uk-overlay-primary uk-position-center-left uk-text-center uk-transition-slide-left ms-5" style="height: 60%; width: 25%">
                    <div class="d-flex flex-column align-items-start">
                        <div class="symbol symbol-80px bg-yellow-800 w-80px mb-2">
                            @if($slide->game()->exists())
                            <img src="{{ Storage::url('/games/logo_'.Str::snake($slide->game->name).'.webp') }}" alt="" class="w-75px">
                            @endif
                        </div>
                        <span class="fs-3 text-white fw-semibold mb-3">{{ $slide->game->name }}</span>
                        <span class="fs-1 text-white fw-bolder text-start">{{ $slide->title }}</span>
                        <div class="d-flex flex-wrap justify-content-center mt-2">
                            <a href="" class="btn btn-outline-light rounded-5" style="width: 100%;">En savoir plus</a>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href uk-slidenav-previous uk-slideshow-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href uk-slidenav-next uk-slideshow-item="next"></a>
        <div class="uk-position-relative">

            <!-- The element which is wrapped goes here -->

            <div class="uk-position-bottom-center uk-position-small">
                <ul class="uk-thumbnav">
                    @foreach($slides as $k => $slide)
                    <li><a href="" uk-slideshow-item="{{ $k }}"><img src="{{ Storage::url('/news/'.$slide->created_at->year.'/'.$slide->created_at->month.'/'.$slide->id.'.webp') }}" width="120px" height="" alt=""></a></li>
                    @endforeach
                </ul>
            </div>

        </div>
    </div>
    <div class="container mb-5">
        <span class="d-inline-block position-relative ms-2">
            <span class="d-inline-block mb-2 fs-2 fw-bold">
                Dernières News
            </span>
            <span class="d-inline-block position-absolute h-8px bottom-0 end-0 start-0 bg-blue-900 translate rounded"></span>
        </span>

        <div class="d-flex flex-wrap justify-content-center align-items-center gap-5 mt-5 news mx-auto " style="width: 80% !important;">
            @foreach($news as $new)
                <div class="uk-animation-toggle news-block">
                    <div  class="uk-card uk-card-default w-250px h-325px me-2 news-block uk-background-cover uk-background-center-center  uk-animation-scale-up rounded-2" style="background-image: url({{ Storage::url('/news/'.$new->created_at->year.'/'.$new->created_at->month.'/fit_'.$new->id.'.webp') }});">
                        <div class="uk-card-media-top">
                            <img src="{{ Storage::url('/news/'.$new->created_at->year.'/'.$new->created_at->month.'/fit_'.$new->id.'.webp') }}" width="" height="" alt="">
                        </div>
                        <div class="uk-card-body ">
                            <a href="" class="bg-opacity-75 bg-grey-200 uk-overlay uk-position-bottom text-dark text-decoration-none fs-6 shadow-lg">
                                @if($new->game()->exists())
                                <div class="badge bg-yellow-800 rounded">{{ $new->game->name }}</div><br>
                                @else
                                <div class="badge bg-primary rounded">Vortech Studio</div><br>
                                @endif
                                <span>{{ $new->title }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="uk-height-large uk-background-cover uk-light uk-flex my-5 shadow-box" uk-parallax="bgy: -200" style="background-image: url('{{ Storage::url('/other/wall_1.png') }}');">
        <h1 class="uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">&nbsp;</h1>
    </div>
    <div class="container mb-5">
        <span class="d-inline-block position-relative ms-2">
            <span class="d-inline-block mb-2 fs-2 fw-bold">
                Nos Produits
            </span>
            <span class="d-inline-block position-absolute h-8px bottom-0 end-0 start-0 bg-blue-900 translate rounded"></span>
        </span>
        <div class="d-flex flex-wrap justify-content-center gap-5 align-items-center">
            @foreach($games as $game)
            <a href="" class="card mb-3 lozad text-decoration-none" style="width: 40%">
                <img src="{{ Storage::url('/games/wall_'.Str::snake($game->name).'.webp') }}" class="card-img-top img-hoverable" alt="...">
                <div class="card-body">
                    <div class="d-flex flex-row align-items-center">
                        <div class="symbol symbol-80px border border-1 bg-grey-200 me-2">
                            <img src="{{ Storage::url('/games/logo_'.Str::snake($game->name).'.webp') }}" alt="" class="w-70px"/>
                        </div>
                        <div class="d-flex flex-column flex-grow-1">
                            <span class="fs-6">{{ $game->name }}</span>
                            <span class="fs-8 text-muted fst-italic">{{ $game->synopsis }}</span>
                        </div>
                        <button href="" class="btn rounded-5 btn-outline-primary">Plus d'info</button>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
    <div class="uk-height-large uk-background-cover uk-light uk-flex mt-5 shadow-box" uk-parallax="bgy: -200" style="background-image: url('{{ Storage::url('/other/wall_2.png') }}');">
        <h1 class="uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
            <div class="d-flex flex-wrap justify-content-around align-items-center">
                <div class="d-flex flex-column align-items-start bg-grey-200 text-dark rounded-2 p-3" style="width: 100%;">
                    <span class="d-inline-block position-relative mb-2">
                        <span class="d-inline-block mb-2 fs-2 fw-bold">
                            A Propos de nous
                        </span>
                        <span class="d-inline-block position-absolute w-45px h-8px bottom-0 end-0 start-0 bg-blue-900 translate rounded"></span>
                    </span>
                    <div class="text-muted fs-4">Build Simulated Universe</div>
                </div>
            </div>
        </h1>
    </div>
@endsection

@section("js")
@endsection
