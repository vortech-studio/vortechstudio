<div class="mb-2">
    <div class="form-check form-switch">
        <input
            class="form-check-input"
            type="checkbox"
            role="switch"
            id="{{ $name }}"
            name="{{ $name }}"
            value="{{ $value }}"
            {{ isset($checked) ? 'checked' : '' }}
        >
        <label class="form-check-label" for="{{ $name }}">{{ $label }}</label>
    </div>
</div>
