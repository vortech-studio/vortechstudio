<div class="mb-2">
    <label for="{{ $name }}" class="form-label">{{ $label }} @if($required !== '') <span class="text-danger">*</span> @endif</label>
    <textarea
        name="{{ $name }}"
        id="{{ $name }}"
        class="form-control"
        {{ $required !== '' ? 'required' : '' }}
        rows="3"></textarea>
</div>
