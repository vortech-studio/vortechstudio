<div class="mb-2">
    <label for="{{ $name }}" class="form-label">{{ $label }} @if($required !== '') <span class="text-danger">*</span> @endif</label>
    <select name="{{ $name }}" id="{{ $name }}" class="form-select" {{ $required !== '' ? 'required' : '' }}>
        <option selected>{{ $selectPlaceholder ?? '-- Selectionner --' }}</option>
        @foreach($cases as $case)
            <option value="{{ $case['value'] }}" {{ isset($value) && $value === $case['value'] ? 'selected' : '' }}>{{ $case['label'] }}</option>
        @endforeach
    </select>
</div>
