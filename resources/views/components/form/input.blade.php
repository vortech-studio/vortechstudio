<div class="mb-2">
    <label for="{{ $name }}" class="form-label">{{ $label }} @if($required !== '') <span class="text-danger">*</span> @endif</label>
    <input
        type="{{ $type }}"
        name="{{ $name }}"
        id="{{ $name }}"
        class="form-control"
        placeholder="{{ $placeholder }}"
        {{ $required !== '' ? 'required' : '' }}
    />
</div>
