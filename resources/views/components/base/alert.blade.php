<div class="alert alert-dismissible d-flex flex-row bg-{{ $type }} align-items-center gap-2 m-2 text-white">
    <i class="fa-solid fa-{{ $icon }} fs-3 me-2"></i>
    <div class="d-flex flex-column flex-grow-1">
        <span class="fw-bolder fs-4">{{ $type_title }}</span>
        <div class="fs-6">{!! $message !!}</div>
    </div>
    <i class="fa-solid fa-xmark fs-4 text-white" data-bs-dismiss="alert"></i>
</div>
