<th wire:click="setOrderField('{{ $name }}')">
    {{ $slot }}
    @if($visible)
        @if($direction === 'ASC')
            <i class="ti ti-caret-up"></i>
        @else
            <i class="ti ti-caret-down"></i>
        @endif
    @endif
</th>
