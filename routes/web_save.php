<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/news', [\App\Http\Controllers\HomeController::class, 'news'])->name('news');
Route::get('/news/{slug}', [\App\Http\Controllers\HomeController::class, 'newsShow'])->name('news.show');
Route::get('/project', [\App\Http\Controllers\HomeController::class, 'project'])->name('project');
Route::get('/project/{slug}', [\App\Http\Controllers\HomeController::class, 'projectShow'])->name('project.show');

Route::get('/support', function () {
    return redirect('https://support.'.config('app.domain'));
})->name('support');

